package com.orhangulcan.mutexexample

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class MainViewModel : ViewModel() {

    private val mutex = Mutex()

    var counterWithoutMutex by mutableStateOf(MockData(0))
        private set

    var counterWithMutex by mutableStateOf(MockData(0))
        private set

    fun increaseCounters() {
        counterWithoutMutex = counterWithoutMutex.copy(mock = 0)
        counterWithMutex = counterWithMutex.copy(mock = 0)

        val jobNoMutex = CoroutineScope(Dispatchers.IO).launch {
            for (i in 1..500) {
                withoutMutex()
            }
        }

        val job2NoMutex = CoroutineScope(Dispatchers.IO).launch {
            for (i in 1..500) {
                withoutMutex()
            }
        }

        val jobWithMutex = CoroutineScope(Dispatchers.IO).launch {
            for (i in 1..500) {
                withMutex()
            }
        }

        val job2WithMutex = CoroutineScope(Dispatchers.IO).launch {
            for (i in 1..500) {
                withMutex()
            }
        }

        viewModelScope.launch {
            joinAll(jobNoMutex, job2NoMutex, jobWithMutex, job2WithMutex)
            println("         No Mutex : $counterWithoutMutex")
            println("       With Mutex : $counterWithMutex")
        }
    }

    private fun withoutMutex() {
        (0 until 10).forEach { _ ->
            counterWithoutMutex = counterWithoutMutex.copy(mock = (counterWithoutMutex.mock) + 1)
        }
    }

    private suspend fun withMutex() {
        mutex.withLock {
            (0 until 10).forEach { _ ->
                counterWithMutex = counterWithMutex.copy(mock = (counterWithMutex.mock) + 1)
            }
        }
    }
}

data class MockData(val mock: Int)

package com.orhangulcan.mutexexample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.orhangulcan.mutexexample.ui.theme.MutexExampleTheme

class MainActivity : ComponentActivity() {
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MutexExampleTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    PageView(
                        { viewModel.increaseCounters() },
                        viewModel.counterWithoutMutex,
                        viewModel.counterWithMutex
                    )
                }
            }
        }
    }
}

@Composable
fun PageView(
    buttonClick: () -> Unit,
    withoutMutexData: MockData,
    withMutexData: MockData,
) {
    Column(modifier = Modifier.fillMaxSize()) {
        Button(
            modifier = Modifier.align(Alignment.CenterHorizontally).padding(16.dp),
            onClick = { buttonClick() }
        ) {
            Text(
                text = "Run",
                modifier = Modifier.padding(bottom = 8.dp),
                style = MaterialTheme.typography.headlineMedium
            )
        }

        Text(
            text = "Without Mutex : " + withoutMutexData.mock.toString(),
            modifier = Modifier.padding(bottom = 8.dp)
                .align(Alignment.CenterHorizontally),
            style = MaterialTheme.typography.bodyMedium
        )
        Text(
            text = "With Mutex : " + withMutexData.mock.toString(),
            modifier = Modifier.padding(bottom = 8.dp)
                .align(Alignment.CenterHorizontally),
            style = MaterialTheme.typography.bodyMedium
        )
    }
}
